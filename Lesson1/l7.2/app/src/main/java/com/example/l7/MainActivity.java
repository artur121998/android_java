package com.example.l7;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void ButtonSentMessage(View view) {

        Spinner spinnerName = findViewById(R.id.spinnerName);
        Spinner spinnerGroup = findViewById(R.id.spinnerGroup);
        EditText editMessage = findViewById(R.id.editMessage);
        TextView tvName = findViewById(R.id.tvName);
        TextView tvGroup = findViewById(R.id.tvGroup);
        TextView tvMessage = findViewById(R.id.tvMessage);

        tvMessage.setText(editMessage.getText().toString());
        tvGroup.setText(spinnerGroup.getSelectedItem().toString());
        tvName.setText(spinnerName.getSelectedItem().toString());
    }
}
